var GraphQLSchema = require('graphql').GraphQLSchema;
var GraphQLObjectType = require('graphql').GraphQLObjectType;
var GraphQLList = require('graphql').GraphQLList;
var GraphQLNonNull = require('graphql').GraphQLNonNull;
var GraphQLID = require('graphql').GraphQLID;
var GraphQLString = require('graphql').GraphQLString;
var GraphQLInt = require('graphql').GraphQLInt;
var GraphQLDate = require('graphql-date');
var UserModel = require('../models').User;


var userType = new GraphQLObjectType({
  name: "user",
  fields: function() {
    return {
      id: {
        type: GraphQLInt
      },
      team: {
        type: GraphQLString
      },
      rank_id: {
        type: GraphQLString
      },
      card_id: {
        type: GraphQLString
      },
      card_code: {
        type: GraphQLString
      },
      call_sign: {
        type: GraphQLString
      },
      spec: {
        type: GraphQLString
      },
      pay_grade: {
        type: GraphQLString
      },
      status: {
        type: GraphQLString
      },
      issue: {
        type: GraphQLString
      },
      expire: {
        type: GraphQLString
      }
    };
  }
});

var queryType = new GraphQLObjectType({
  name: 'Query',
  fields: function () {
    return {
      users: {
        type: new GraphQLList(userType),
        resolve: function () {
          const users = UserModel.findAll({
            order: [
              ['createdAt', 'DESC']
            ],
          })
          if (!users) {
            throw new Error('Error')
          }
          return users
        }
      },
      user: {
        type: userType,
        args: {
          id: {
            name: 'id',
            type: GraphQLInt
          }
        },
        resolve: function (root, params) {
          const userDetails = UserModel.findByPk(params.id)
          if (!userDetails) {
            throw new Error('Error')
          }
          return userDetails
        }
      }
    }
  }
});

var mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: function () {
    return {
      addUser: {
        type: userType,
        args: {
          team: {
            type: new GraphQLNonNull(GraphQLString)
          },
          rank_id: {
            type: new GraphQLNonNull(GraphQLString)
          },
          card_id: {
            type: new GraphQLNonNull(GraphQLString)
          },
          card_code: {
            type: new GraphQLNonNull(GraphQLString)
          },
          call_sign: {
            type: new GraphQLNonNull(GraphQLString)
          },
          spec: {
            type: new GraphQLNonNull(GraphQLString)
          },
          status: {
            type: new GraphQLNonNull(GraphQLString)
          },
          pay_grade: {
            type: new GraphQLNonNull(GraphQLString)
          },
          issue: {
            type: new GraphQLNonNull(GraphQLString)
          },
          expire: {
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve: function (root, params) {
          const userModel = new UserModel(params);
          const newUser = userModel.save();
          if (!newUser) {
            throw new Error('Error');
          }
          return newUser
        }
      },
      updateUser: {
        type: userType,
        args: {
          id: {
            name: 'id',
            type: new GraphQLNonNull(GraphQLInt)
          },
          team: {
            type: new GraphQLNonNull(GraphQLString)
          },
          rank_id: {
            type: new GraphQLNonNull(GraphQLString)
          },
          card_id: {
            type: new GraphQLNonNull(GraphQLString)
          },
          card_code: {
            type: new GraphQLNonNull(GraphQLString)
          },
          call_sign: {
            type: new GraphQLNonNull(GraphQLString)
          },
          spec: {
            type: new GraphQLNonNull(GraphQLString)
          },
          status: {
            type: new GraphQLNonNull(GraphQLString)
          },
          pay_grade: {
            type: new GraphQLNonNull(GraphQLString)
          },
          issue: {
            type: new GraphQLNonNull(GraphQLString)
          },
          expire: {
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve(root, params) {
          return UserModel
            .findByPk(params.id)
            .then(user => {
              if (!user) {
                throw new Error('Not found');
              }
              return user
                .update({
                  team: params.team || user.team,
                  rank_id: params.rank_id || user.rank_id,
                  card_id: params.card_id || user.card_id,
                  card_code: params.card_code || user.card_code,
                  call_sign: params.call_sign || user.call_sign,
                  spec: params.spec || user.spec,
                  pay_grade: params.pay_grade || user.pay_grade,
                  status: params.status || user.status,
                  issue: params.issue || user.issue,
                  expire: params.expire || user.expire
                })
                .then(() => { return user; })
                .catch((error) => { throw new Error(error); });
            })
            .catch((error) => { throw new Error(error); });
        }
      },
      removeUser: {
        type: userType,
        args: {
          id: {
            type: new GraphQLNonNull(GraphQLInt)
          }
        },
        resolve(root, params) {
          return UserModel
            .findByPk(params.id)
            .then(user => {
              if (!user) {
                throw new Error('Not found');
              }
              return user
                .destroy()
                .then(() => { return user; })
                .catch((error) => { throw new Error(error); });
            })
            .catch((error) => { throw new Error(error); });
        }
      }
    }
  }
});

module.exports = new GraphQLSchema({query: queryType, mutation: mutation});

