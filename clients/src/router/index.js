import VueRouter from 'vue-router'
import UserList from '@/components/UserList'
import ShowUser from '@/components/ShowUser'
import AddUser from '@/components/AddUser'
import EditUser from '@/components/EditUser'

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'UserList',
      component: UserList
    },
    {
      path: '/show-user/:id',
      name: 'ShowUser',
      component: ShowUser
    },
    {
      path: '/add-user',
      name: 'AddUser',
      component: AddUser
    },
    {
      path: '/edit-user/:id',
      name: 'EditUser',
      component: EditUser
    }
  ]
})