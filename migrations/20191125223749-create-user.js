'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      team: {
        type: Sequelize.STRING
      },
      call_sign: {
        type: Sequelize.STRING
      },
      spec: {
        type: Sequelize.STRING
      },
      pay_grade: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      issue: {
        type: Sequelize.STRING
      },
      expire: {
        type: Sequelize.STRING
      },
      rank_id: {
        type: Sequelize.STRING
      },
      card_id: {
        type: Sequelize.STRING
      },
      card_code: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};