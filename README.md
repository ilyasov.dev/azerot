
Необходимо:
=============   
```json
node -v
v10.15.1
npm -v
6.8.0
yarn -v
1.10.1
psql --version
psql (PostgreSQL) 11.5
```

База данных:
=================
```json
psql postgres --u postgres
postgres-# CREATE ROLE azerot WITH LOGIN PASSWORD 'azerot1231';
postgres-# ALTER ROLE azerot CREATEDB;
postgres-# \q
psql postgres -U azerot
postgres=> CREATE DATABASE greed;
postgres=> GRANT ALL PRIVILEGES ON DATABASE greed TO azerot;
postgres=> \q
```

В проекте
======================
```json
npm i
```
из корня проекта
```json
sequelize db:migrate
```
После этого будет залита схема в базу
```json
node bin/www
```
Будет доступно API для добавления, редактирования и удаления пользователей

http://localhost:3000/graphql



Получить пользователя
```json
{
  user(id: 1) {
      id
      team
      call_sign
      spec
      pay_grade
      status
      issue
      expire
      rank_id
      card_id
      card_code
  }
}
```

Добавить пользователя
```json
mutation {
  addUser(
    team: "as3434d",
    rank_id: "asd",
    card_id: "asd",
    card_code: "q23123",
    call_sign: "asd",
    spec: "asdadasd",
    status: "asasd",
    pay_grade: "qeq34",
    issue: "2019-02-26T13:55:39.160Z",
    expire: "2019-02-26T13:55:39.160Z") {
    id
  }
}
```
Удалить
```json
mutation {
  removeUser(id: 1) {
    id
  }
}
```
Клиент в очень сыром виде
```json
cd client/ && npm i && npm run build && npm run serve
```