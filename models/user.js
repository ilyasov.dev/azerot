'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    team: DataTypes.STRING,
    call_sign: DataTypes.STRING,
    spec: DataTypes.STRING,
    pay_grade: DataTypes.STRING,
    status: DataTypes.STRING,
    issue: DataTypes.STRING,
    expire: DataTypes.STRING,
    rank_id: DataTypes.STRING,
    card_id: DataTypes.STRING,
    card_code: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};